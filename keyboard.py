#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct  9 11:23:22 2021

@author: prime
"""

from tkinter import *
from tkinter import ttk, font
from chapters import chapters, section

current_section = 0

def set_lesson_title(*args):
    try:
        cu_chapter = chapter_title.get()
        
        lesson_title.set(chapters[cu_chapter][0])
        type_box.focus()
        
    except ValueError:
        pass
    

def set_chapter_title(*args):
    try:
        chapter_title.set(section[current_section])
    except ValueError:
        pass
    
    
lesson_index = 0   
def lesson_update(*args):
    global lesson_index
    chapter_index = len(chapters[chapter_title.get()])
    
    if lesson_index + 1 is chapter_index:
        update_chapter()
        lesson_index = 0
        
    else:
        lesson_index = lesson_index + 1
        typingvar.set("")
        cu_chapter = chapter_title.get()
        lesson_title.set(chapters[cu_chapter][lesson_index])
        key_starter()



def update_chapter(*args):
    next_chapter()
    
    
def next_chapter(*args):
    try:
        global current_section
        if current_section == len(section) - 1:
            current_section = 0
        else:
            current_section = current_section + 1
            
        chapter = section[current_section]
        chapter_title.set(chapter)
        set_lesson_title()
        key_starter()

    except ValueError:
        pass
        


def prev_chapter(*args):
    try:
        global current_section
        if current_section  < 1:
            current_section = len(section) - 1
        else:
            current_section = current_section - 1
            
        chapter = section[current_section]
        chapter_title.set(chapter)
        set_lesson_title()
        key_starter()
    except ValueError:
        pass
    
    
    

    

root = Tk()
root.title("Typing Beginner")
root.geometry("830x600")


chapter = ttk.Frame(root, padding="10", borderwidth="1", width=30)
chapter.grid(column=1, row=0, sticky=E)

prev_btn = ttk.Button(chapter, text="<<", command=prev_chapter)
prev_btn.grid(column=0, row=0)

chapter_title = StringVar()
chapter_label = ttk.Label(chapter, textvariable=chapter_title, padding="10 10")
chapter_label.grid(column=1, row=0)

next_btn = ttk.Button(chapter, text=">>", command=next_chapter)
next_btn.grid(column=2, row=0)


lesson = ttk.Frame(root, padding="10", borderwidth=1, width=90 )
lesson.grid(columnspan=3, row=1)

lesson_title = StringVar()
lesson_label = ttk.Label(lesson, 
                         background="black", 
                         foreground="gold",  
                         padding="0 10 0 10", 
                         textvariable=lesson_title, 
                         borderwidth=3, 
                         width=80,
                         font=("Pyidaungsu", 13))
lesson_label.grid(column=1, row=1, pady=5)


type_frame = ttk.Frame(root, padding="10")
type_frame.grid(columnspan=3, row=2)

typingvar = StringVar()
type_box = ttk.Entry(type_frame, width=80, textvariable=typingvar, 
                     font=("Pyidaungsu", 13))
type_box.grid(column=1, row=0)

keybar1 = ttk.Frame(root, padding="10", width=100)
keybar1.grid(columnspan=2, row=4, pady=1)


keyQQ = ttk.Label(keybar1, text="ဎ     ~\n     `", width=6, padding="5 10 0 10", relief="sunken" )
keyQQ.grid(column=0, row=4, padx=1, sticky=(E,W))

key1 = ttk.Label(keybar1, text="ဍ     !\n၁     1", width=6, padding="5 10 0 10", relief="sunken" )
key1.grid(column=1, row=4, padx=1, sticky=(E,W))

key2 = ttk.Label(keybar1, text="ၒ     @\n၂     2", width=6, padding="5 10 0 10", relief="sunken" )
key2.grid(column=2, row=4, padx=1, sticky=(E,W))

key3 = ttk.Label(keybar1, text="ဋ     #\n၃     3", width=6, padding="5 10 0 10", relief="sunken" )
key3.grid(column=3, row=4, padx=1, sticky=(E,W))

key4 = ttk.Label(keybar1, text="ၓ     $\n၄     4", width=6, padding="5 10 0 10", relief="sunken" )
key4.grid(column=4, row=4, padx=1, sticky=(E,W))

key5 = ttk.Label(keybar1, text="ၔ     %\n၅     5", width=6, padding="5 10 0 10", relief="sunken" )
key5.grid(column=5, row=4, padx=1, sticky=(E,W))

key6 = ttk.Label(keybar1, text="ၕ     ^\n၆     6", width=6, padding="5 10 0 10", relief="sunken" )
key6.grid(column=6, row=4, padx=1, sticky=(E,W))

key7 = ttk.Label(keybar1, text="ရ     &\n၇     7", width=6, padding="5 10 0 10", relief="sunken" )
key7.grid(column=7, row=4, padx=1, sticky=(E,W))

key8 = ttk.Label(keybar1, text="*     *\n၈     8", width=6, padding="5 10 0 10", relief="sunken" )
key8.grid(column=8, row=4, padx=1, sticky=(E,W))

key9 = ttk.Label(keybar1, text="(     (\n၉     9", width=6, padding="5 10 0 10", relief="sunken" )
key9.grid(column=9, row=4, padx=1, sticky=(E,W))

key0 = ttk.Label(keybar1, text=")     )\n၀     0", width=6, padding="5 10 0 10", relief="sunken" )
key0.grid(column=10, row=4, padx=1, sticky=(E,W))

keyMns = ttk.Label(keybar1, text="_     \n-     ", width=6, padding="5 10 0 10", relief="sunken" )
keyMns.grid(column=11, row=4, padx=1, sticky=(E,W))

keyEqu = ttk.Label(keybar1, text="+     \n=     ", width=6, padding="5 10 0 10", relief="sunken" )
keyEqu.grid(column=12, row=4, padx=1, sticky=(E,W))

keyBSP = ttk.Label(keybar1, text="Backspace", width=8, padding="5 17 0 17", relief="sunken" )
keyBSP.grid(column=13, row=4, padx=1, sticky=(E,W))

keybar2 = ttk.Frame(root, padding="5", width=100)
keybar2.grid(columnspan=2, row=5, pady=1)

keyTAB = ttk.Label(keybar2, text="Tab", width=8, padding="5 17 0 17", relief="sunken" )
keyTAB.grid(column=0, row=5, padx=1, sticky=(E,W))

keyQ = ttk.Label(keybar2, text="ဈ\nဆ     Q", width=6, padding="5 10 0 10", relief="sunken" )
keyQ.grid(column=1, row=5, padx=1, sticky=(E,W))

keyW = ttk.Label(keybar2, text="ဝ\nတ     W", width=6, padding="5 10 0 10", relief="sunken" )
keyW.grid(column=2, row=5, padx=1, sticky=(E,W))

keyE = ttk.Label(keybar2, text="ဣ\nန     E", width=6, padding="5 10 0 10", relief="sunken" )
keyE.grid(column=3, row=5, padx=1, sticky=(E,W))

keyR = ttk.Label(keybar2, text="၎\nမ     R", width=6, padding="5 10 0 10", relief="sunken" )
keyR.grid(column=4, row=5, padx=1, sticky=(E,W))

keyT = ttk.Label(keybar2, text="ဤ\nအ     T", width=6, padding="5 10 0 10", relief="sunken" )
keyT.grid(column=5, row=5, padx=1, sticky=(E,W))

keyY = ttk.Label(keybar2, text="၌\nပ     Y", width=6, padding="5 10 0 10", relief="sunken" )
keyY.grid(column=6, row=5, padx=1, sticky=(E,W))

keyU = ttk.Label(keybar2, text="ဥ\nက     U", width=6, padding="5 10 0 10", relief="sunken" )
keyU.grid(column=7, row=5, padx=1, sticky=(E,W))

keyI = ttk.Label(keybar2, text="၍\nင     I", width=6, padding="5 10 0 10", relief="sunken" )
keyI.grid(column=8, row=5, padx=1, sticky=(E,W))

keyO = ttk.Label(keybar2, text="ဿ\nသ     O", width=6, padding="5 10 0 10", relief="sunken" )
keyO.grid(column=9, row=5, padx=1, sticky=(E,W))

keyP = ttk.Label(keybar2, text="ဏ\nစ     P", width=6, padding="5 10 0 10", relief="sunken" )
keyP.grid(column=10, row=5, padx=1, sticky=(E,W))

keyBL = ttk.Label(keybar2, text="ဧ\nဟ     [", width=6, padding="5 10 0 10", relief="sunken" )
keyBL.grid(column=11, row=5, padx=1, sticky=(E,W))

keyBR = ttk.Label(keybar2, text="ဪ\nဩ     ]", width=6, padding="5 10 0 10", relief="sunken" )
keyBR.grid(column=12, row=5, padx=1, sticky=(E,W))

keyBS = ttk.Label(keybar2, text="ၑ\n၏    \ ", width=6, padding="5 10 0 10", relief="sunken" )
keyBS.grid(column=13, row=5, padx=1, sticky=(E,W))


keybar3 = ttk.Frame(root, padding="5", width=100)
keybar3.grid(columnspan=2, row=6, pady=1)

keyCAP = ttk.Label(keybar3, text="CapLock", width=11, padding="5 17 0 17", relief="sunken" )
keyCAP.grid(columnspan=1, row=6, padx=1, sticky=(E,W))

keyA = ttk.Label(keybar3, text="ဗ\nေ     A", width=6, padding="5 10 0 10", relief="sunken" )
keyA.grid(column=1, row=6, padx=1, sticky=(E,W))

keyS = ttk.Label(keybar3, text=" ှ\n ျ     S", width=6, padding="5 10 0 10", relief="sunken" )
keyS.grid(column=2, row=6, padx=1, sticky=(E,W))

keyD = ttk.Label(keybar3, text=" ီ\n ိ     D", width=6, padding="5 10 0 10", relief="sunken" )
keyD.grid(column=3, row=6, padx=1, sticky=(E,W))

keyF = ttk.Label(keybar3, text=" ္\n ်     F", width=6, padding="5 10 0 10", relief="sunken" )
keyF.grid(column=4, row=6, padx=1, sticky=(E,W))

keyG = ttk.Label(keybar3, text=" ွ\n ါ     G", width=6, padding="5 10 0 10", relief="sunken" )
keyG.grid(column=5, row=6, padx=1, sticky=(E,W))

keyH = ttk.Label(keybar3, text=" ံ\n ့     H", width=6, padding="5 10 0 10", relief="sunken" )
keyH.grid(column=6, row=6, padx=1, sticky=(E,W))

keyJ = ttk.Label(keybar3, text=" ဲ \nြ     J", width=6, padding="5 10 0 10", relief="sunken" )
keyJ.grid(column=7, row=6, padx=1, sticky=(E,W))

keyK = ttk.Label(keybar3, text="ဒ\nု     K", width=6, padding="5 10 0 10", relief="sunken" )
keyK.grid(column=8, row=6, padx=1, sticky=(E,W))

keyL = ttk.Label(keybar3, text="ဓ\nူ     L", width=6, padding="5 10 0 10", relief="sunken" )
keyL.grid(column=9, row=6, padx=1, sticky=(E,W))

keyCol = ttk.Label(keybar3, text="ဂ    :\nး     ;", width=6, padding="5 10 0 10", relief="sunken" )
keyCol.grid(column=10, row=6, padx=1, sticky=(E,W))

keyQt = ttk.Label(keybar3, text="'     \"\n''     '", width=6, padding="5 10 0 10", relief="sunken" )
keyQt.grid(column=11, row=6, padx=1, sticky=(E,W))

keyENT = ttk.Label(keybar3, text="Enter", width=10, padding="5 17 0 17", relief="sunken" )
keyENT.grid(column=12, row=6, padx=1, sticky=(E,W))


keybar4 = ttk.Frame(root, padding="5", width=100)
keybar4.grid(columnspan=2, row=7, pady=1)

keyShL = ttk.Label(keybar4, text="Shift", width=14, padding="5 17 0 17", relief="sunken" )
keyShL.grid(column=0, row=7, padx=1, sticky=(E,W))

keyZ = ttk.Label(keybar4, text="ဇ\nဖ     Z", width=6, padding="5 10 0 10", relief="sunken" )
keyZ.grid(column=1, row=7, padx=1, sticky=(E,W))

keyX = ttk.Label(keybar4, text="ဌ\nထ     X", width=6, padding="5 10 0 10", relief="sunken" )
keyX.grid(column=2, row=7, padx=1, sticky=(E,W))

keyC = ttk.Label(keybar4, text="ဃ\nခ     C", width=6, padding="5 10 0 10", relief="sunken" )
keyC.grid(column=3, row=7, padx=1, sticky=(E,W))

keyV = ttk.Label(keybar4, text="ဠ\nလ     V", width=6, padding="5 10 0 10", relief="sunken" )
keyV.grid(column=4, row=7, padx=1, sticky=(E,W))

keyB = ttk.Label(keybar4, text="ယ\nဘ     B", width=6, padding="5 10 0 10", relief="sunken" )
keyB.grid(column=5, row=7, padx=1, sticky=(E,W))

keyN = ttk.Label(keybar4, text="ဉ\nည     N", width=6, padding="5 10 0 10", relief="sunken" )
keyN.grid(column=6, row=7, padx=1, sticky=(E,W))

keyM = ttk.Label(keybar4, text="ဦ\nာ     M", width=6, padding="5 10 0 10", relief="sunken" )
keyM.grid(column=7, row=7, padx=1, sticky=(E,W))

keyComa = ttk.Label(keybar4, text="၊     <\n,     ,", width=6, padding="5 10 0 10", relief="sunken" )
keyComa.grid(column=8, row=7, padx=1, sticky=(E,W))

keyDot = ttk.Label(keybar4, text="။     >\n.     .", width=6, padding="5 10 0 10", relief="sunken" )
keyDot.grid(column=9, row=7, padx=1, sticky=(E,W))

keyQu = ttk.Label(keybar4, text="?\n/", width=6, padding="5 10 0 10", relief="sunken" )
keyQu.grid(column=10, row=7, padx=1, sticky=(E,W))

keyShR = ttk.Label(keybar4, text="Shift", width=14, padding="5 17 0 17", relief="sunken" )
keyShR.grid(column=11, row=7, padx=1, sticky=(E,W))


keybar5 = ttk.Frame(root, padding="5", width=100)
keybar5.grid(columnspan=2, row=8, pady=1)

keyCtrlL = ttk.Label(keybar5, text="Ctrl", width=9, padding="5 17 0 17", relief="sunken" )
keyCtrlL.grid(column=0, row=8, padx=1, sticky=(E,W))

keyAltL = ttk.Label(keybar5, text="Alt", width=9, padding="5 17 0 17", relief="sunken" )
keyAltL.grid(column=1, row=8, padx=1, sticky=(E,W))

keySPACE = ttk.Label(keybar5, text=" ", width=60, padding="5 17 0 17", relief="sunken" )
keySPACE.grid(column=2, row=8, padx=1, sticky=(E,W))

keyAltR = ttk.Label(keybar5, text="Alt", width=9, padding="5 17 0 17", relief="sunken" )
keyAltR.grid(column=3, row=8, padx=1, sticky=(E,W))

keyCtrlR = ttk.Label(keybar5, text="Ctrl", width=9, padding="5 17 0 17", relief="sunken" )
keyCtrlR.grid(column=4, row=8, padx=1, sticky=(E,W))



key_dict = {
        " " : keySPACE,
        "ၐ" : keyQQ,
        "ဎ" : [keyQQ, keyShR],
        "၁" : key1,
        "ဍ" : [key1, keyShR],
        "၂" : key2,
        "ၒ" : [key2, keyShR],
        "၃" : key3,
        "ဋ" : [key3, keyShR],
        "၄" : key4,
        "ၓ" : [key4, keyShR],
        "၅" : key5,
        "ၔ" : [key5, keyShR],
        "၆" : key6,
        "ၕ" : [key6, keyShR],
        "၇" : key7,
        "ရ" : [key7, keyShR],
        "၈" : key8,
        "*" : [key8, keyShR],
        "၉" : key9,
        "(" : [key9, keyShR],
        "၀" : key0,
        ")" : [key0, keyShR],
        "-" : keyMns,
        "=" : keyEqu,
        "\x08" : keyBSP,
        "ဆ" : keyQ,
        "ဈ" : [keyQ, keyShR],
        "တ" : keyW,
        "ဝ" : [keyW, keyShR],
        "န" : keyE,
        "ဣ" : [keyE, keyShR],
        "မ" : keyR,
        "၎" : [keyR, keyShR],
        "အ" : keyT,
        "ဤ" : [keyT, keyShR],
        "ပ" : keyY,
        "၌" : [keyY, keyShL],
        "က" : keyU,
        "ဥ" : [keyU, keyShL],
        "င" : keyI,
        "၍" : [keyI, keyShL],
        "သ" : keyO,
        "ဿ" : [keyO, keyShL],
        "စ" : keyP,
        "ဏ" : [keyP, keyShL],
        "ဟ" : keyBL,
        "ဧ" : [keyBL, keyShL],
        "ဩ" : keyBR,
        "ဪ" : [keyBR, keyShL],
        "၏" : keyBS,
        "ၑ" : [keyBS, keyShL],
        "ေ" : keyA,
        "ဗ" : [keyA, keyShR],
        "ျ" : keyS,
        "ှ" : [keyS, keyShR],
        "ိ" : keyD,
        "ီ" : [keyD, keyShR],
        "်" : keyF,
        "္" : [keyF, keyShR],
        "ါ" : keyG,
        "ွ" : [keyG, keyShR],
        "့" : keyH,
        "ံ" : [keyH, keyShL],
        "ြ" : keyJ,
        "ဲ" : [keyJ, keyShL],
        "ု" : keyK,
        "ဒ" : [keyK, keyShL],
        "ူ" : keyL,
        "ဓ" : [keyL, keyShL],
        "း" : keyCol,
        "ဂ" : [keyCol, keyShL],
        "'" : keyQt,
        "ဖ" : keyZ,
        "ဇ" : [keyZ, keyShR],
        "ထ" : keyX,
        "ဌ" : [keyX, keyShR],
        "ခ" : keyC,
        "ဃ" : [keyC, keyShR],
        "လ" : keyV,
        "ဠ" : [keyV, keyShR],
        "ဘ" : keyB,
        "ယ" : [keyB, keyShL],
        "ည" : keyN,
        "ဉ" : [keyN, keyShL],
        "ာ" : keyM,
        "ဦ" : [keyM, keyShL],
        "," : keyComa,
        "၊" : [keyComa, keyShL],
        "." : keyDot,
        "။" : [keyDot, keyShL],
        "/" : keyQu,
        "?" : [keyQu, keyShL],
        }
    

cu_words = []


def key_starter(*args):
    global cu_words, index
    
    index = 0
    typingvar.set("")
    for i in key_dict:
        key_normal(key_dict.get(i))
        
    cu_words = lesson_title.get()
    start_word = cu_words[index]
    key_ck = key_dict.get(start_word)
    key_setter(key_ck)




def key_setter(keeys):
    if isinstance(keeys, list):
        for keey in keeys:
            keey.configure(background="cyan")
    else:
        keeys.configure(background="cyan")
   
    

wrong_key = []

def key_warnner(keeys):
    if isinstance(keeys, list):
        for keey in keeys:
            keey.configure(background="red")
            
    else:
        keeys.configure(background="red")

        
    


def key_normal(keeys):
    if isinstance(keeys, list):
        for keey in keeys:
            keey.configure(background="")
    else:
        keeys.configure(background="")
    

index = 0

def key_checker(keypress, *args):
    global index
    global wrong_key
    typekey = keypress.char

    
    if keypress.char == '\x08':
        if len(wrong_key) < 1:
            print('out of range')
        else:
            last_wrong_key = wrong_key[-1] 
            key_normal(key_dict.get(last_wrong_key))
            wrong_key.pop(-1)
            for i in wrong_key:
                key_warnner(key_dict.get(i))

    else:
        if typingvar.get() == cu_words:
            key_normal(key_dict.get(cu_words[index]))
            lesson_update()
            
        else:
            if typekey == cu_words[index]:
                                   
                key_normal(key_dict.get(cu_words[index]))
                typingvar.set(cu_words[:index+1])
                if wrong_key:
                    
                    for i in wrong_key:
                        key_normal(key_dict.get(i))
                    wrong_key.clear()
                    
                index = index + 1    
                key_setter(key_dict.get(cu_words[index]))
                
            else:            
                key_warnner(key_dict.get(typekey))           
                wrong_key.append(keypress.char)

        
        
        
        
test_f = ttk.Frame(root, padding="0 10 0 10")
test_f.grid()

l_test = ttk.Label(test_f, text='Author: fb.me/prime.kpw')
l_test.grid(column=0)




set_chapter_title()
set_lesson_title()

type_box.bind("<FocusIn>", key_starter)
root.bind("<KeyPress>",lambda e: key_checker(e))
root.mainloop()